import app from "./app.js";
import {connectDB} from "./db.js";

const conection = connectDB();
app.listen(2799);
console.log('Server port '+ 2799);