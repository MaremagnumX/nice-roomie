import { Router } from "express";
import { authRequired } from "../middlewares/validateToken.js";
import { loginSchema, registerSchema } from "../schemas/validate.schema.js";
import { validateSchema } from "../middlewares/validator.middleware.js";
import { login, logout, profile, register, verifyToken } from "../controllers/auth.controller.js";

const router = Router();

router.post('/login', validateSchema(loginSchema), login);
router.post('/register', validateSchema(registerSchema), register);
router.get('/profile', authRequired ,profile);
router.post('/logout', logout);
router.get('/verify', verifyToken);

export default router;