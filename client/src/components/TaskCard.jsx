import { Link } from "react-router-dom";
import { useTasksContext } from "../context/TaskContex";
import { MdDeleteForever, MdEditSquare } from "react-icons/md";

import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
dayjs.extend(utc);

export default function TaskCard({ task }) {

  const { deleteTask } = useTasksContext();
  return (
    <div className="bg-zinc-800 max-w-bg p-5 rounded-md m-2">
      <header className="flex justify-between">
        <h1 className=" text-2xl font-semibold text-white">{task.title}</h1>
        <div className="flex items-center">
          <Link
          to={`/task/${task._id}`} 
          className="p-2 mx-2 bg-white rounded-full">
            <MdEditSquare className="text-zinc-800" />
          </Link>
          <button
            onClick={() => {
              deleteTask(task._id)
            }}
            className="p-2 mx-2 bg-red-500 rounded-full">
            <MdDeleteForever />
          </button>
        </div>
      </header>
      <p className="text-slate-300">{task.description}</p>
      <p>{dayjs(task.date).utc().format('DD/MM/YYYY')}</p>
    </div>
  )
}
