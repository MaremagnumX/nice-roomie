import { useAuth } from "../context/AuthContext";
import { Link } from "react-router-dom";

export default function Navbar() {

  const { isAuthenticated, logout, user } = useAuth();
  return (
    <nav className="bg-zinc-700 mb-3 flex justify-between py-5 px-10 rounded-b-lg">
      <Link to={isAuthenticated ? '/tasks' : '/'}>
        <h1 className="text-2xl font-bold">Task Manager</h1>
      </Link>
      <ul className="flex gap-x-2">
        {isAuthenticated ? (
          <>
            <p>Hola {user.username}</p>
            <p>|</p>
            <li>
              <Link to={'/'}>Inicio</Link>
            </li>
            <li>
              <Link to={'/tasks'}>Tareas</Link>
            </li><li>
              <Link to={'/add-task'}>Crear tarea</Link>
            </li>
            <li>
              <Link to={'/profile'}>Perfil</Link>
            </li>
            <li>
              <Link to={'/'} onClick={() => {
                logout();
              }}>Salir</Link>
            </li>
          </>
        ) : (
          <>
            <li>
              <Link to={'/login'}>Login</Link>
            </li>
            <li>
              <Link to={'/register'}>Register</Link>
            </li>
          </>
        )}

      </ul>

    </nav>
  )
}
