import { createTaskRequest, getTaskRequest, getTasksRequest, deleteTaskRequest, updateTaskRequest } from "../api/task";
import { createContext, useContext, useState } from "react";

const TaskContext = createContext();

export const useTasksContext = () => {
  const context = useContext(TaskContext);

  if (!context) {
    throw new Error("useTasksContext debe estar dentro de un TaskProvider");
  }

  return context;
};

export function TaskProvider({ children }) {
  const [tasks, setTasks] = useState([]);

  const getTasks = async () => {
    const res = await getTasksRequest();
    try {
      setTasks(res.data);
    } catch (error) {
      console.error(error);
    }
  }

  const getTask = async (id) => {
    try {
      const res = await getTaskRequest(id);
      return res.data;
    } catch (error) {
      console.error(error);
    }
  }

  const createTask = async (task) => {
    const res = await createTaskRequest(task);
    console.log("Hola"+res);
  }

  const deleteTask = async (id) => {
    try {
      const res = await deleteTaskRequest(id);
      if (res.status === 204) setTasks(tasks.filter(task => task._id !== id));
    } catch (error) {
      console.log(error);
    }

  }

  const updateTask = async (id, task) => {
    try {
      const res = await updateTaskRequest(id, task);
      console.log(res);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <TaskContext.Provider
      value={{
        tasks,
        createTask,
        getTask,
        getTasks,
        deleteTask,
        updateTask,
      }}
    >
      {children}
    </TaskContext.Provider>
  );
}