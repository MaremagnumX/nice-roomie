import { useEffect } from "react";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../context/AuthContext";

function LoginPage() {
  
  const navigate = useNavigate();
  const { register, handleSubmit, formState: { errors } } = useForm();
  const { signin, errors: singinErrors, isAuthenticated } = useAuth();

  useEffect(() => {
    if (isAuthenticated) navigate('/tasks')
  }, [isAuthenticated, navigate])

  const onSubmit = handleSubmit(data => {
    signin(data);
  })

  return (
    <div className="flex h-[calc(100vh-100px)] items-center justify-center">
      <div className="bg-zinc-800 max-w-ms p-10 rounded-md">
        <h1 className="mb-3 text-2xl font-bold">Inicia sesión</h1>
        {
          singinErrors ? singinErrors.map((error, i) => (
            <div className="mb-3 p-2 bg-red-500 rounded-md" key={i}>{error}</div>
          )) : <div></div>
        }
        <form onSubmit={onSubmit}>
          {errors.email && (<p className="text-red-500 font-semibold">El email es requerido!</p>)}
          <input type="email" {...register('email', { required: true })}
            className="mb-3 w-full bg-zinc-700 text-white px-4 py-2 rounded-md"
            placeholder="Email"
          />

          {errors.password && (<p className="text-red-500 font-semibold">La contraseña es requerida!</p>)}
          <input type="password" {...register('password', { required: true })}
            className="mb-3 w-full bg-zinc-700 text-white px-4 py-2 rounded-md"
            placeholder="Contraseña"
          />
          <button
            type="submit"
            className="mb-3 w-full bg-zinc-400 px-4 py-2 rounded-md hover:bg-zinc-300 active:bg-zinc-200 font-semibold text-black">
            Ingresar
          </button>
        </form>
        <p className="text-end">¿Aún no tienes cuenta?<br/><Link className="underline text-sky-200" to="/register">¡Registrate!</Link></p>
      </div>
    </div>
  )
}

export default LoginPage;