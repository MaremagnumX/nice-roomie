import { useEffect } from "react";
import TaskCard from "../components/TaskCard";
import { useTasksContext } from "../context/TaskContex";

export default function TasksPage() {
  const { getTasks, tasks } = useTasksContext();

  useEffect(() => {
    getTasks();
  }, [])

  return (
    <div>
      <h1 className="m-5 text-5xl text-white text-center font-bold">Tasks page</h1>
      <div className="grid sm:grid-cols-2 md:grid-cols-3 gap-2">
        {
          tasks.map(task => (
            <TaskCard task={task} key={task._id} />
          ))
        }
      </div>
    </div>
  )
}