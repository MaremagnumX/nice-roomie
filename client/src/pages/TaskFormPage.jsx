import { useForm } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import { useTasksContext } from "../context/TaskContex"
import { useEffect } from "react";

import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
dayjs.extend(utc);

export default function TaskFormPage() {

  const { register, handleSubmit, setValue } = useForm();
  const { createTask, getTask, updateTask } = useTasksContext();
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    async function loadTask() {
      if (id) {
        const task = await getTask(id);
        console.log(task);
        setValue('title', task.title);
        setValue('description', task.description);
        setValue('date', dayjs.utc(task.date).format("YYYY-MM-DD"));
      }
    }
    loadTask();
  }, [getTask, id, setValue])


  const onSubmit = handleSubmit((data) => {
    const dataValid = {
      ...data,
      date: data.date? dayjs.utc(data.date).format() : dayjs.utc().format()
    }
    if (id) {
      updateTask(id, dataValid);
    } else {
      createTask(dataValid);
    }
    navigate('/tasks');
  });

  return (
    <div>
      <div className="flex flex-col h-[calc(100vh-100px)] items-center justify-center">
        <h1 className="m-5 text-4xl text-white text-center font-bold">{id ? 'Editar tarea' : 'Crear nueva tarea'}</h1>
        <form onSubmit={onSubmit} className="bg-zinc-800 max-w-ms p-10 rounded-md">
          <label htmlFor="title">Titulo</label>
          <input type="text" placeholder="Titulo" {...register("title")} autoFocus className="mb-3 w-full bg-zinc-700 text-white px-4 py-2 rounded-md" />
          <label htmlFor="title">Descripción</label>
          <label htmlFor="description"></label>
          <textarea rows={3} placeholder="Descripción" {...register("description")} className="mb-3 w-full bg-zinc-700 text-white px-4 py-2 rounded-md" />
          <label htmlFor="date">Fecha</label>
          <input type="date" {...register('date')} className="mb-3 w-full bg-zinc-700 text-white px-4 py-2 rounded-md" />
          <button type="submit" className="w-full bg-zinc-400 px-4 py-2 rounded-md hover:bg-zinc-300 active:bg-zinc-200 font-semibold text-black">Crear tarea</button>
        </form>
      </div>
    </div>

  )
}
